import React from "react";
import ToolbarOptions from "../Toolbar/ToolbarOptions";

import "./SideDrawer.css";
const SideDrawer = (props) => {
	const { categoriesList } = props;
	let drawerClasses = "side_drawer";
	if (props.show) {
		drawerClasses = "side_drawer open";
	}
	return (
		<nav className={drawerClasses}>
			<ToolbarOptions options={categoriesList} />
		</nav>
	);
};

export default SideDrawer;
