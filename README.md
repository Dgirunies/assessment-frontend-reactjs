# Frontend Webjump

Este projeto consiste em uma reprodução funcional do layout apresentado pela Webjump e o mesmo foi criado usando [Create React App](https://github.com/facebook/create-react-app).

O projecto utiliza a biblioteca React para a sua concepção.

## Inicializando o Projeto

Após clonado ou transferido o projecto para o disco local do computador, o usuário deverá executar os seguintes comandos no terminal (após ter o mesmo navegado ao directório que foi transferido):

### `npm install`

O comando `npm install` será responsável por instalar todas as dependências do projecto. Após executado o usuário deverá executar o comando `npm start` que servirá a página no navegador padrão do mesmo.

O `npm start` servirá a página no modo de desenvolvimento na rota [http://localhost:3000](http://localhost:3000)

## Instruções Adicionais

1. As informações do projecto dependem das API's disponíveis no [Projecto Webjump](http://localhost:3000) inicial e para a sua visualização é necessário que o mesmo projecto também esteja em execução e a ser servido no endereço [http://localhost:8888](http://localhost:8888)

2. Os filtros da Sidebar funcionam de acordos com os filtros disponibilizados pelas API's o que faz com que algum dos filtros disponíveis não sejam apropriados para um determinado grupo de produdos.

**Note: O projecto pode ser encontrado [aqui](https://bitbucket.org/Dgirunies/assessment-frontend-reactjs/src/master/).**
