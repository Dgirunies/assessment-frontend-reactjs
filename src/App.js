import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import HomeScreen from "./screens/HomeScreen";
import CatalogueScreen from "./screens/CatalogueScreen";

import Toolbar from "./componets/Toolbar/Toolbar";
import Footer from "./componets/Footer/Footer";

import "./App.css";
import SideDrawer from "./componets/SideDrawer/SideDrawer";
import Backdrop from "./componets/Backdrop/Backdrop";
function App() {
	const [categoriesList, setCategoriesList] = useState([]);
	const [sideDrawerOpen, setSideDrawerOpen] = useState(false);
	useEffect(() => {
		fetch(`http://localhost:8888/api/V1/categories/list`)
			.then((res) => {
				if (res.ok) {
					return res.json();
				}
			})
			.then((resData) => {
				setCategoriesList(resData);
			});
	}, []);

	const drawerToggleClickHandler = () => {
		setSideDrawerOpen((prevState) => !prevState);
	};

	const drawerCloseHandler = () => {
		setSideDrawerOpen(false);
	};

	return (
		<Router className="App">
			<Toolbar
				categoriesList={categoriesList}
				drawerClickHandler={drawerToggleClickHandler}
			/>
			<SideDrawer categoriesList={categoriesList} show={sideDrawerOpen} />
			{sideDrawerOpen && <Backdrop click={drawerCloseHandler} />}
			<main className="mainContent">
				<Switch>
					<Route path="/" exact>
						<HomeScreen categoriesList={categoriesList} />
					</Route>
					<Route path="/:catalogue">
						<CatalogueScreen />
					</Route>
				</Switch>
			</main>
			<Footer />
		</Router>
	);
}

export default App;
