import React from "react";

import { Link } from "react-router-dom";

const ToolbarOptions = (props) => {
	const { options } = props;
	return (
		<ul>
			<li>
				<Link to="/">Pagina Inicial</Link>
			</li>
			{options?.items?.map((item) => (
				<li key={item.path}>
					<Link to={`/${item.path}`} key={item.id.toString()}>
						{item.name}
					</Link>
				</li>
			))}
			<li>
				<Link to="/Contato">Contato</Link>
			</li>
		</ul>
	);
};

export default ToolbarOptions;
