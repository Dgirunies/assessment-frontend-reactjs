import React, { useCallback, useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import CatalogueItems from "../componets/Catalogue/CatalogueItems";

import Filters from "../componets/Filters/Filters";
import PreviewSchema from "../componets/PreviewSchema/PreviewSchema";

import "./CatalogueScreen.css";

const CatalogueScreen = (props) => {
	const { catalogue } = useParams();

	const [currentCatalogue, setCurrentCatalogue] = useState(0);
	const [catalogueItems, setCatalogueItems] = useState([]);
	const [filteredCatalogueItems, setFilteredCatalogueItems] = useState([]);
	const [colorFilter, setColorFilter] = useState(null);
	const [genderFilter, setGenderFilter] = useState(null);

	useEffect(() => {
		fetch(`http://localhost:8888/api/V1/categories/list`)
			.then((res) => {
				if (res.ok) {
					return res.json();
				}
			})
			.then((resData) => {
				const currentCatalogue = resData.items.filter(
					(item) => item.path === catalogue
				)[0];
				setCurrentCatalogue(currentCatalogue);
			});
	}, [catalogue]);

	useEffect(() => {
		// Clearing Filters
		setColorFilter(null);
		setGenderFilter(null);
		setFilteredCatalogueItems(null);
		if (!currentCatalogue?.id) {
			return;
		}
		fetch(`http://localhost:8888/api/V1/categories/${currentCatalogue.id}`)
			.then((res) => {
				if (res.ok) {
					return res.json();
				}
			})
			.then((resData) => {
				setCatalogueItems(resData);
			});
	}, [currentCatalogue]);

	const filtersHandler = useCallback((color, gender) => {
		let filteredItems = [];

		setColorFilter(color);
		setGenderFilter(gender);

		if (color) {
			filteredItems = catalogueItems.items.filter(
				(item) =>
					item.filter.filter((filter) => filter["color"] === color)?.length > 0
			);
		}
		if (gender) {
			filteredItems = catalogueItems.items.filter(
				(item) =>
					item.filter.filter((filter) => filter["gender"] === gender)?.length >
					0
			);
		}
		setFilteredCatalogueItems({ items: [...filteredItems] });
	});

	if (!currentCatalogue?.name) {
		return <p>Desculpa, infelizmente não encontramos a pagina que procura.</p>;
	}

	return (
		<React.Fragment>
			<div className="catalogueMain__Top">
				<p className="catalogueMain__Top_path-Paragraph">
					<Link to="/">Pagina Inicial </Link> {">"}{" "}
					<span>{currentCatalogue.name}</span>
				</p>
			</div>
			<div className="catalogueMain">
				<div className="catalogueMain__sideMenu grayTxt">
					<div className="sideContainer">
						<h2 className="redTxt">FILTRE POR</h2>
						<h4 className="bbBlueTxt">CATEGORIAS</h4>
						<ul>
							<li>
								<p>Roupas</p>
							</li>
							<li>
								<p>Sapatos</p>
							</li>
							<li>
								<p>Acessórios</p>
							</li>
						</ul>
						<Filters
							filters={catalogueItems?.filters}
							setFilters={filtersHandler}
						/>
						<h4 className="bbBlueTxt">TIPO</h4>
						<ul>
							<li>
								<p>Roupas</p>
							</li>
							<li>
								<p>Sapatos</p>
							</li>
							<li>
								<p>Acessórios</p>
							</li>
						</ul>
					</div>
				</div>
				<div className="catalogueMain__content">
					<div>
						<p className="catalogueMain__content_title redTxt">
							{currentCatalogue.name}
						</p>
					</div>
					<div className="catalogueMain__content_sort-n-filter">
						<PreviewSchema setPreviewSchema={() => {}} />
						<div className="spacer" />
						<div className="catalogueMain__content_sort-n-filter-text">
							<p>ORDENAR POR</p>
						</div>
						<div className="filterSelect">
							<select>
								<option>Preço</option>
							</select>
						</div>
					</div>
					{catalogueItems?.items?.length && (
						<CatalogueItems
							catalogueItems={
								filteredCatalogueItems?.items?.length ||
								colorFilter ||
								genderFilter
									? filteredCatalogueItems
									: catalogueItems
							}
						/>
					)}
				</div>
			</div>
		</React.Fragment>
	);
};

export default CatalogueScreen;
