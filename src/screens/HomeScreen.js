import React from "react";

import ToolbarOptions from "../componets/Toolbar/ToolbarOptions";

import "./HomeScreen.css";
const HomeScreen = (props) => {
	const { categoriesList } = props;
	return (
		<div className="homeMain">
			<div className="homeMain__sideMenu">
				<ToolbarOptions options={categoriesList} />
			</div>
			<div className="homeMain__content">
				<div className="homeMain__content_banner"></div>
				<div className="homeMain__content_text">
					<p className="homeMain__content_text_title">Seja Bem-Vindo</p>
					<p className="homeMain__content_text_description">
						Lorem ipsum, dolor sit amet consectetur adipisicing elit. Recusandae
						quibusdam inventore cum autem aliquam cumque omnis id impedit quam
						expedita, voluptates illum praesentium veniam ducimus similique
						magni earum. Nam, iste. Lorem, ipsum dolor sit amet consectetur
						adipisicing elit. Illum consequuntur mollitia ipsam error pariatur
						soluta provident delectus? Itaque non accusamus, saepe quae suscipit
						dignissimos necessitatibus. Quam amet reiciendis fugiat neque! Lorem
						ipsum dolor sit, amet consectetur adipisicing elit. Animi
						dignissimos soluta unde alias explicabo? Corporis temporibus nam
						dignissimos voluptas! Architecto sit dolores numquam est excepturi
						earum ullam ad porro enim?
					</p>
				</div>
			</div>
		</div>
	);
};

export default HomeScreen;
