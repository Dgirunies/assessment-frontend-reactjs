import React, { useState, useEffect, useCallback } from "react";

import "./Filters.css";
const Filters = (props) => {
	const { filters } = props;
	const [currentFilters, setCurrentFilters] = useState([]);
	const [colorFilter, setColorFilter] = useState(null);
	const [genderFilter, setGenderFilter] = useState(null);

	useEffect(() => {
		if (filters?.length) {
			const newFilters = [];
			setCurrentFilters([]);
			for (const filter of filters) {
				if (filter.color) {
					newFilters.push(filter.color);
				} else if (filter.gender) {
					newFilters.push(filter.gender);
				}
				setCurrentFilters(newFilters);
			}
		}
	}, [filters]);

	useEffect(() => {
		props.setFilters(colorFilter, genderFilter);
	}, [colorFilter, genderFilter]);

	const filterHandler = useCallback((type, value) => {
		if (type === "color") {
			if (colorFilter === value) {
				setColorFilter(null);
			} else {
				setColorFilter(value);
			}
		} else if (type === "gender") {
			if (genderFilter === value) {
				setGenderFilter(null);
			} else {
				setGenderFilter(value);
			}
		}
	});

	return (
		<div className="filtersContainer">
			{currentFilters.includes("Cor") && (
				<React.Fragment>
					<h4 className="bbBlueTxt">CORES</h4>
					<div className="colorContainer">
						<div
							className={`redBg ${
								colorFilter === "Vermelha"
									? "colorContainerSelected"
									: undefined
							}`}
							onClick={filterHandler.bind(null, "color", "Vermelha")}
						></div>

						<div
							className={`orangeBg ${
								colorFilter === "Laranja" ? "colorContainerSelected" : undefined
							}`}
							onClick={filterHandler.bind(null, "color", "Laranja")}
						></div>

						<div
							className={`bbBlueBg ${
								colorFilter === "Azul" ? "colorContainerSelected" : undefined
							}`}
							onClick={filterHandler.bind(null, "color", "Azul")}
						></div>
					</div>
				</React.Fragment>
			)}
			{currentFilters.includes("Gênero") && (
				<React.Fragment>
					<h4 className="bbBlueTxt">GÊNERO</h4>
					<ul>
						<li>
							<div>
								<p
									className={
										genderFilter === "Masculina"
											? "txt_filter_selected"
											: undefined
									}
									onClick={filterHandler.bind(null, "gender", "Masculina")}
								>
									Masculina
								</p>
							</div>
						</li>
						<li>
							<div>
								<p
									className={
										genderFilter === "Feminina"
											? "txt_filter_selected"
											: undefined
									}
									onClick={filterHandler.bind(null, "gender", "Feminina")}
								>
									Feminina
								</p>
							</div>
						</li>
					</ul>
				</React.Fragment>
			)}
		</div>
	);
};

export default Filters;
