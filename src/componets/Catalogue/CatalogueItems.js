import React from "react";

import "./CatalogueItems.css";
const CatalogueItems = (props) => {
	const { catalogueItems } = props;
	return (
		<React.Fragment>
			{catalogueItems?.items?.length ? (
				<React.Fragment>
					<div className="catalogueMain__content_products-list">
						{catalogueItems.items.map((product) => (
							<div
								className="catalogueMain__content_products"
								key={product.sku}
							>
								<div className="catalogueMain__content_products_image-container">
									<img src={`/${product.image}`} alt="" />
								</div>
								<div className="catalogueMain__content_products_description-container">
									<p className="catalogueMain__content_products_description-container-title grayTxt">
										{product.name}
									</p>
									<p className="catalogueMain__content_products_description-container-price redTxt">
										{product.specialPrice && (
											<span className="grayTxt catalogueMain__content_products_description-container-price-old">
												R${product.price}
											</span>
										)}
										R$
										{product.specialPrice
											? product.specialPrice
											: product.price}
									</p>
								</div>
								<button className="catalogueMain__content_products_description-container-btn customButtom">
									COMPRAR
								</button>
							</div>
						))}
					</div>
					<div className="pagination__elements">
						<div className="bbBlueTxt">{"<"}</div>
            			{/* <p className="grayTxt">1</p>
						<p className="grayTxt">2</p> */}
            			<p className="redTxt">1</p>
            			{/* <p className="grayTxt">4</p>
						<p className="grayTxt">5</p> */}
            			<div className="bbBlueTxt">{">"}</div>
					</div>
				</React.Fragment>
			) : (
				<p style={{ textAlign: "center" }}>
					{" "}
					Desculpe, não encontramos produtos para o seu pedido. Tente escolher
					outra categoria ou limpar os filtros{" "}
				</p>
			)}
		</React.Fragment>
	);
};

export default CatalogueItems;
