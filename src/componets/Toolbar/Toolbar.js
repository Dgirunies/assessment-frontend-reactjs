import React from "react";

import ToolbarOptions from "./ToolbarOptions";
import DrawerToggleButton from "../SideDrawer/DrawerToggleButton";

import "./Toolbar.css";
import SearchButton from "../SearchButton/SearchButton";

const Toolbar = (props) => {
	const { categoriesList } = props;

	return (
		<header className="toolbar">
			<div className="toolbar__auth">
				<div className="spacer" />
				<div>
					<p className="toolbar__auth_text">
						<span> Acesse a sua conta </span> ou <span>Cadastre-se</span>
					</p>{" "}
				</div>{" "}
			</div>{" "}
			<div className="toolbar__top">
				<div>
					<DrawerToggleButton click={props.drawerClickHandler} />
				</div>
				<div className="sm-spacer" />
				<div className="toolbar__top_logo">
					<img src={"/media/webjump.png"} alt="logo" />
				</div>{" "}
				<div className="spacer" />
				<div className="toolbar__top_search">
					<input type="text" />{" "}
					<button className="customButtom"> BUSCAR </button>{" "}
				</div>{" "}
				<SearchButton />
			</div>{" "}
			<nav className="toolbar__nav">
				<ToolbarOptions options={categoriesList} />
			</nav>{" "}
		</header>
	);
};

export default Toolbar;
