import React, { useState } from "react";
import { useEffect } from "react/cjs/react.development";

import ActiveGrid from "../../assets/media/ui/gridActive.png";
import InactiveGrid from "../../assets/media/ui/gridInactive.png";

import ActiveList from "../../assets/media/ui/listActive.png";
import InactiveList from "../../assets/media/ui/listInactive.png";

import "./PreviewSchema.css";

const PreviewSchema = (props) => {
	const [previewSchema, setPreviewSchema] = useState("grid");

	useEffect(() => {
		props.setPreviewSchema(previewSchema);
	}, [previewSchema]);

	const previewSchemaHandler = (value) => {
		setPreviewSchema(value);
	};

	return (
		<div className="schema-buttons">
			<img
				src={previewSchema === "grid" ? ActiveGrid : InactiveGrid}
				alt="grid"
				onClick={previewSchemaHandler.bind(null, "grid")}
			/>
			<img
				src={previewSchema === "list" ? ActiveList : InactiveList}
				alt="list"
				onClick={previewSchemaHandler.bind(null, "list")}
			/>
		</div>
	);
};

export default PreviewSchema;
