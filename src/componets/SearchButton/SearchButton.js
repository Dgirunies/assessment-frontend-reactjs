import React from "react";

import SearchIcon from "../../assets/media/ui/searchIcon.png";

import "./SearchButton.css";
const SearchButton = (props) => {
	return (
		<button className="search_button" onClick={props.click}>
			<img src={SearchIcon} alt="search" />
		</button>
	);
};

export default SearchButton;
